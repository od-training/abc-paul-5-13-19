import { Component, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DashboardService } from '../dashboard.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-filter-views',
  templateUrl: './filter-views.component.html',
  styleUrls: ['./filter-views.component.css']
})
export class FilterViewsComponent implements OnDestroy {
  nameFilter = new FormControl();
  filterSubscription: Subscription;

  constructor(dashboardService: DashboardService) {
    dashboardService.searchFilter.subscribe(filterValue =>
      this.nameFilter.setValue(filterValue)
    );
    this.filterSubscription = this.nameFilter.valueChanges.subscribe(filter =>
      dashboardService.updateSearch(filter)
    );
  }

  ngOnDestroy() {
    this.filterSubscription.unsubscribe();
  }
}
