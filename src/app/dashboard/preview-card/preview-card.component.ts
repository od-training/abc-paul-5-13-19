import { Component, Input } from '@angular/core';

import { Video } from '../dashboard.types';

@Component({
  selector: 'app-preview-card',
  templateUrl: './preview-card.component.html',
  styleUrls: ['./preview-card.component.css']
})
export class PreviewCardComponent {
  @Input() video: Video | undefined;
}
