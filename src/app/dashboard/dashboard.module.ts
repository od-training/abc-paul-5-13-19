import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { PreviewListComponent } from './preview-list/preview-list.component';
import { ViewBreakdownComponent } from './view-breakdown/view-breakdown.component';
import { FilterViewsComponent } from './filter-views/filter-views.component';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { PreviewCardComponent } from './preview-card/preview-card.component';

@NgModule({
  declarations: [
    DashboardComponent,
    PreviewListComponent,
    ViewBreakdownComponent,
    FilterViewsComponent,
    VideoPlayerComponent,
    PreviewCardComponent
  ],
  imports: [CommonModule, DashboardRoutingModule, ReactiveFormsModule]
})
export class DashboardModule {}
