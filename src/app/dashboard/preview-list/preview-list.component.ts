import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Video } from '../dashboard.types';

@Component({
  selector: 'app-preview-list',
  templateUrl: './preview-list.component.html',
  styleUrls: ['./preview-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreviewListComponent {
  @Input() list: Video[] = [];
  @Input() selectedId: string | undefined;
  @Output() selectedVideo = new EventEmitter<Video>();

  selectVideo(video: Video) {
    this.selectedVideo.emit(video);
  }
}
