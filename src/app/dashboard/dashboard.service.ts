import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video } from './dashboard.types';
import { FormControl } from '@angular/forms';
import { combineLatest, of, merge, Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  videos: Observable<Video[]>;
  searchFilter: Observable<string>;

  constructor(
    private httpClient: HttpClient,
    activedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.searchFilter = activedRoute.queryParams.pipe(
      map(params => params['filter'])
    );

    this.videos = this.searchFilter.pipe(
      map(filter => (!filter ? '' : filter)),
      switchMap(filter =>
        this.httpClient
          .get<Video[]>('https://api.angularbootcamp.com/videos')
          .pipe(
            map(videos =>
              videos.filter(
                video =>
                  video.title.toLowerCase().indexOf(filter.toLowerCase()) >= 0
              )
            )
          )
      )
    );
  }

  updateSearch(filter: string) {
    this.router.navigate([], { queryParams: { filter } });
  }

  updateSelectedVideo() {}
}
