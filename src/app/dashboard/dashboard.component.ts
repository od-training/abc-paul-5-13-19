import { Component } from '@angular/core';

import { Video } from './dashboard.types';
import { DashboardService } from './dashboard.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  videos: Observable<Video[]>;
  selectedVideo: Video | undefined;

  constructor(dashboardService: DashboardService) {
    this.videos = dashboardService.videos.pipe(
      tap(videos => (this.selectedVideo = videos[0]))
    );
  }

  setSelectedVideo(video: Video) {
    this.selectedVideo = video;
  }
}
